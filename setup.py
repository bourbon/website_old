from setuptools import find_packages, setup


setup(
    name='Website',
    version='0.0',
    description='My personal website',
    license='LICENSE',
    packages=find_packages(),
    install_requires=[
        'flask',
        'sqlalchemy',
    ],
)
