import configparser
import os


INSTALL_PATH = os.path.join(os.path.dirname(__file__), os.pardir)
CONFIG_PATH = os.path.join(INSTALL_PATH, 'website.conf')

env_config = configparser.ConfigParser()
env_config.read(CONFIG_PATH)


class ProductionConfig(object):
    DEBUG = False
    TESTING = False
    DATABASE_URI = env_config.get('Core', 'db_uri', fallback=f'sqlite:////{INSTALL_PATH}/website.sqlite')
    SECRET_KEY = env_config.get('Core', 'secret', fallback='Super Secret Key')


class DevelopmentConfig(ProductionConfig):
    DEBUG = True
    TESTING = True
