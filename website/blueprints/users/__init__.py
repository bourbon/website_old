from flask import Blueprint

from website.decorators import login_required

from .models import User, check_credentials
from .parsers import get_login_args

app = Blueprint('users', __name__)


@app.route('/')
@login_required
def index():
    # get models
    # format a page
    # return the page
    pass


@app.route('/login')
def login():
    args = get_login_args()
    if check_credentials(args.email, args.password):
        # TODO - set Flask g for session
        # TODO - redirect to /
        pass
    # TODO - login failed banner
