from flask_restful import reqparse


def get_login_args():
    login_parser = reqparse.RequestParser()
    login_parser.add_argument('email')
    login_parser.add_argument('password')
    return login_parser.parse_args()
