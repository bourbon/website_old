import datetime

from hashlib import sha256
from sqlalchemy import Column, Date, Integer, String

from website import Base, engine, session


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False)
    password = Column(String, nullable=False)
    is_admin = Column(Integer, default=0)
    last_login = Column(Date, default=datetime.datetime.now)


def check_credentials(email, password):
    u = session.query(User).filter_by(email=email)
    h = sha256(password).hexdigest()
    if getattr(u, 'password', None) == h:
        return True
    return False


Base.metadata.create_all(engine)
