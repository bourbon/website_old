from flask import Blueprint, g, redirect, url_for

from website.decorators import login_required


app = Blueprint('admin', __name__)


@app.before_request()
def check_login():
    if not getattr(g, 'email'):
        return redirect(url_for('users.login'))


@app.route('/')
@login_required
def index():
    pass
