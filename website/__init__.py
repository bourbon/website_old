import logging

from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from website.blueprints.users import app as admin_app
from website.blueprints.projects import app as projects_app
from website.blueprints.resume import app as resume_app


logger = logging.getLogger(__name__)
engine = create_engine('sqlite:///website.sqlite3', convert_unicode=True)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()


def create_app(cfg):
    logger.info('initializing website app')
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(cfg)

    logger.info('registering blueprints')
    app.register_blueprint(admin_app, url_prefix='/users')
    app.register_blueprint(projects_app, url_prefix='/projects')
    app.register_blueprint(resume_app, url_prefix='/resume')

    @app.errorhandler(404)
    def page_not_found(e):
        return e

    @app.errorhandler(500)
    def server_error(e):
        return e

    return app
